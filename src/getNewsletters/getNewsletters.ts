import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda'
import {
  DynamoDB,
  ScanCommandOutput
} from '@aws-sdk/client-dynamodb'
import {HistoryEntry} from '../common/types'
import {
  failure,
  success
} from '../common/util'


const tableName = process.env.HISTORY_TABLE

export const fromItem = (item): HistoryEntry => {
  const entry = {}
  Object.entries(item).forEach(([key, value]) => entry[key] = value[Object.keys(value)[0]])
  return entry as HistoryEntry
}

// noinspection JSUnusedLocalSymbols
export const getNewsletters = (dynamoDB: DynamoDB) => async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  try {
    const result: ScanCommandOutput = await dynamoDB.scan({TableName: tableName})
    return success(result.Items.map(fromItem))
  } catch (e) {
    return failure(500, e.message)
  }
}