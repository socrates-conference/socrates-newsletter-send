process.env.HISTORY_TABLE = 'history'
import {APIGatewayProxyEvent} from 'aws-lambda'
import {getNewsletters} from './getNewsletters'
import {FakeDynamoDB} from '../mock/dynamoDB'
import {DynamoDB} from '@aws-sdk/client-dynamodb'


describe('GET /newsletters', () => {
  let fakeDynamoDB: FakeDynamoDB
  beforeEach(() => {
    fakeDynamoDB = new FakeDynamoDB()
  })

  it('should return empty list', async () => {
    const result = await getNewsletters(fakeDynamoDB as unknown as DynamoDB)({pathParameters: undefined} as APIGatewayProxyEvent)
    expect(result.statusCode).toEqual(200)
    expect(fakeDynamoDB.table).toEqual('history')
    expect(JSON.parse(result.body)).toEqual([])
  })

  describe('when a history entry exists', () => {
    const ENTRY: { template: { S: string }; id: { S: string }; timestamp: { S: string } } = {
      id: {S: '1'},
      template: {S: 'template'},
      timestamp: {S: '1997/12/12 01:01:01'}
    }
    beforeEach(() => {
      fakeDynamoDB._history.set(JSON.stringify({id: {S: '1'}}), ENTRY)
    })

    it('should return list of one', async () => {
      const result = await getNewsletters(fakeDynamoDB as unknown as DynamoDB)({} as APIGatewayProxyEvent)
      expect(result.statusCode).toEqual(200)
      expect(fakeDynamoDB.table).toEqual('history')
      expect(JSON.parse(result.body)).toEqual([{id: '1', template: 'template', timestamp: '1997/12/12 01:01:01'}])
    })
  })
})