import {getNewsletters} from './getNewsletters'
import {DynamoDB} from '@aws-sdk/client-dynamodb'

export const handler = getNewsletters(new DynamoDB({region: 'eu-central-1'}))