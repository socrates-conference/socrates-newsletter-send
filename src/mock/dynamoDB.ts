import {ScanOutput} from '@aws-sdk/client-dynamodb'

type DynamoString = { S: string }
type DynamoSubscriber = { id: DynamoString, name: DynamoString, email: DynamoString }
type DynamoHistoryEntry = { id: DynamoString, template: DynamoString, timestamp: DynamoString }

export class FakeDynamoDB {

  public subscribers: Map<string, DynamoSubscriber> = new Map()
  public _history: Map<string, DynamoHistoryEntry> = new Map()
  public table: String

  async putItem(props: { TableName: string, Item: DynamoHistoryEntry }) {
    const {TableName, Item} = props
    this.table = TableName
    this._history.set(JSON.stringify({'id': Item.id}), Item)
  }

  async scan({TableName}): Promise<ScanOutput> {
    this.table = TableName
    if (TableName === 'subscribers') {
      return {Items: [...this.subscribers.values()] as any[]}
    } else {
      return {Items: [...this._history.values()] as any[]}
    }
  }
}