export type UUID = string
export type Subscriber = {
  name: string,
  email: string
}
export type HistoryEntry = {
  id: UUID,
  template: string,
  timestamp: string
}