import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda'
import {v4 as uuid} from 'uuid'
import {
  DynamoDB,
  PutItemInput
} from '@aws-sdk/client-dynamodb'
import {
  HistoryEntry,
  Subscriber,
  UUID
} from '../common/types'
import {
  failure,
  success
} from '../common/util'
import {SES} from '@aws-sdk/client-ses'
import axios from 'axios'

const historyTableName = process.env.HISTORY_TABLE
const subscribersUrl = process.env.SUBSCRIBERS_URL
const source: string = process.env.SOURCE_EMAIL

type NewsletterPayload = HistoryEntry & { key?: UUID, value?: any }

const valueType = (value: any) => {
  if (typeof value === 'string') {
    return 'S'
  }
  if (typeof value === 'number') {
    return 'N'
  }
  if (typeof value === 'boolean') {
    return 'BOOL'
  }
}

const toItemInput = (body: string) => {
  const payload: NewsletterPayload = JSON.parse(body)
  const {key, value} = payload
  const item = key !== undefined ? value : payload
  const Key = key !== undefined ? key : uuid()
  const Item = {id: {S: Key}}
  Object.entries(item).forEach(([key, value]) => Item[key] = {[valueType(value)]: value})
  return {TableName: historyTableName, Item}
}

async function sendBulkMail(ses: SES, template: string, subscribers): Promise<void> {
  await ses.sendBulkTemplatedEmail({
    Template: template,
    Source: source,
    Destinations: subscribers
      .map(s => ({
        Destination: {ToAddresses: [s.email]},
        ReplacementTemplateData: `{"name":"${s.name}"}`
      })),
    DefaultTemplateData: '{"name":"Subscriber"}'
  })
}

export const postNewsletter = (ses: SES, dynamoDB: DynamoDB) => async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  try {
    const input: PutItemInput = toItemInput(event.body)
    const template: string = JSON.parse(event.body).template
    axios.defaults.headers['x-api-key'] = process.env.API_KEY
    const subscribers:Subscriber[] = (await axios.get(subscribersUrl)).data
    if (subscribers.length > 0) {
      const batchSize = 40
      let start = 0;
      while(start < subscribers.length) {
        const batch = subscribers.slice(start, start+batchSize)
        await sendBulkMail(ses, template, batch)
        start += batchSize
      }
      await dynamoDB.putItem(input)
    }
    return success()
  } catch (e) {
    return e.type === 'ValidationError'
           ? failure(400, 'The input arguments were invalid.')
           : failure(500, e.message)
  }
}
