import axios from 'axios'

jest.mock('axios');

process.env.HISTORY_TABLE = 'history'
process.env.SUBSCRIBERS_URL = '/subscribers'
process.env.SOURCE_EMAIL = 'info@socrates-conference.de'

import {FakeSES} from '../mock/ses'
import {APIGatewayProxyResult} from 'aws-lambda'
import {DynamoDB} from '@aws-sdk/client-dynamodb'
import {postNewsletter} from './postNewsletter'
import {FakeDynamoDB} from '../mock/dynamoDB'
import {SES} from '@aws-sdk/client-ses'

describe('POST /newsletters', () => {
  let result: APIGatewayProxyResult
  const fakeDynamoDB: FakeDynamoDB = new FakeDynamoDB()
  const fakeSES: FakeSES = new FakeSES()

  beforeAll(async () => {
    const event: any = {body: JSON.stringify({'template': 'template', 'timestamp': '1997/12/12 01:01:01'})}
    // @ts-ignore
    axios.get.mockImplementation(()=> Promise.resolve({data:[{id:'1', email:'some@body.com', name:'somebody'}]}))
    result = await postNewsletter(fakeSES as unknown as SES, fakeDynamoDB as unknown as DynamoDB)(event)
  })

  it('should return status 200',  () => {
    expect(result.statusCode).toEqual(200)
  })
  it('should send from provided email', () => {
    expect(fakeSES.source).toEqual('info@socrates-conference.de')
  })

  it('should send to provided destination', () => {
    expect(fakeSES.destinations)
      .toEqual([{Destination: {ToAddresses: ['some@body.com']}, ReplacementTemplateData: '{"name":"somebody"}'}])
  })

  it('should store a valid history entry', async () => {
    expect(fakeDynamoDB.table).toEqual('history')
    expect((await fakeDynamoDB.scan({TableName: process.env.HISTORY_TABLE})).Items[0].template).toEqual({'S': 'template'})
  })
})